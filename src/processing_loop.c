// Processing loop (contains geofence code as an example only)
// processing_loop():  Top-level thread function
// update_set_points():  Geofencing reference points (may be useful as a guide)
// processing_loop_initialize():  Set up data processing / geofence variables
// read_config:  Read configuration parameters from a file (customize please)
//
#define EXTERN extern
#define PROC_FREQ 100 // in Hz

// for testing the PID control 

#include "../include/quadcopter_main.h"
void read_config(char *);
void PID_test_Command_handler(struct state* localstate);

int processing_loop_initialize();


/*
 * State estimation and guidance thread 
//  Set point update in the below, this part for state estimation and according to the 
//different state gives different set point or opening(closing) the grapper
//need to consider the situation that do not catch the bar, need to fly up to maintain its position and catch again












*/
void *processing_loop(void *data){

  int hz = PROC_FREQ;
  processing_loop_initialize();

  // Local copies
  struct motion_capture_obs *mcobs[2];  // [0] = new, [1] = old
  struct state localstate;

  while (1) {  // Main thread loop

    pthread_mutex_lock(&mcap_mutex);
    mcobs[0] = mcap_obs;
    if(mcap_obs[1].time > 0) mcobs[1] = mcap_obs+1;
    else               mcobs[1] = mcap_obs;
    pthread_mutex_unlock(&mcap_mutex);

    localstate.time = mcobs[0]->time;
    localstate.pose[0] = mcobs[0]->pose[0]; // x position
    localstate.pose[1] = mcobs[0]->pose[1]; // y position
    localstate.pose[2] = mcobs[0]->pose[2]; // z position / altitude
    localstate.pose[3] = mcobs[0]->pose[5]; // yaw angle
    localstate.pose[4] = 0;
    localstate.pose[5] = 0;
    localstate.pose[6] = 0;
    localstate.pose[7] = 0;
    
    // Estimate velocities from first-order differentiation
    double mc_time_step = mcobs[0]->time - mcobs[1]->time;
    if(mc_time_step > 1.0E-7 && mc_time_step < 1){
      localstate.pose[4] = (mcobs[0]->pose[0]-mcobs[1]->pose[0])/mc_time_step;
      localstate.pose[5] = (mcobs[0]->pose[1]-mcobs[1]->pose[1])/mc_time_step;
      localstate.pose[6] = (mcobs[0]->pose[2]-mcobs[1]->pose[2])/mc_time_step;
      localstate.pose[7] = (mcobs[0]->pose[5]-mcobs[1]->pose[5])/mc_time_step;
    }

  // Geofence activation code
  // turn fence on --> may be useful to control your autonomous controller
    if(localstate.fence_on == 0 &&
       (localstate.pose[0] > x_pos_fence)){
      // ADD YOUR CODE HERE
    } // end if (localstate.fence_on == 0)

    // else if fence is on and has been on for desired length of time
    else if(localstate.fence_on == 1 && localstate.time - localstate.time_fence_init >= fence_penalty_length){
      // turn fence off
      localstate.fence_on = 0;
      printf("Fence Off\n");

    } // end else if (localstate.fence_on == 1 && timed out)

    else if(localstate.fence_on == 1){
      // update desired state
      update_set_points(&localstate,1);
    }

    // Copy to global state (minimize total time state is locked by the mutex)
    pthread_mutex_lock(&state_mutex);
    memcpy(state, &localstate, sizeof(struct state));
    pthread_mutex_unlock(&state_mutex);

    usleep(1000000/hz);

  } // end while processing_loop()

  return 0;
}

// if the quadrotor reaches the current waypoint, set the next waypoint
int update_set_points(struct state* localstate, int first_time)
{
    float threshold = 0.05;
    float threshold_yaw = 0.05;
    float* set_points = localstate->set_points;
    double* pose = localstate->pose;
    double time = localstate->time;
    double init_time = localstate->time_fence_init;
    if(abs(pose[0]-set_points[0])<threshold && abs(pose[1]-set_points[1])<threshold && abs(pose[2]-set_points[2])<threshold)
    {
	// go down (before grasp)
        if (!GoUp && current_set_point_idx < num_set_points){
            current_set_point_idx += 1;
            for(int i=0;i<8;++i)
	    {
                localstate->set_points[i] = list_of_set_points[current_set_point_idx][i];
            }
            printf("way reach, load next way point\n");
	}
        // go up (after grasp)
	else if (GoUp && current_set_point_idx > 0){
            current_set_point_idx -= 1;
            for(int i=0;i<8;++i)
	    {
                localstate->set_points[i] = list_of_set_points[current_set_point_idx][i];
            }
            printf("way reach, load next way point\n");
	}
	// gripper grasping
        else if (!GraspDone && current_set_point_idx = num_set_points){
	    if (OpenGripper){
	    	set_dynamixel();
	    OpenGripper = false;
	    }
            set_dynamixel();

	    // if catch the bar, set GraspDone to true ???
	    if ()
	        GraspDone = true;
	    else
		OpenGripper = true;
	// if not, it will stay in the same area or go to previous waypoint. so do nothing
	}
	// gripper ungrasping
	else if (GraspDone && current_set_point_idx = num_set_points){
	    // if grasp finished, first release bar, then set GoUp flag	    
	    set_dynamixel();
	    GoUp = true;
	}
    }

  //Add your code here to generate proper reference state for the controller

  return 0;
}


/**
 * processing_loop_initialize()
*/
int processing_loop_initialize()
{
  // Initialize state struct
  state = (state_t*) calloc(1,sizeof(*state));
  state->time = ((double)utime_now())/1000000;
  memset(state->pose,0,sizeof(state->pose));

  // Read configuration file
  char blah[] = "config.txt";
  read_config(blah);

  mcap_obs[0].time = state->time;
  mcap_obs[1].time = -1.0;  // Signal that this isn't set yet

  // Initialize Altitude Velocity Data Structures
  memset(diff_z, 0, sizeof(diff_z));
  memset(diff_z_med, 0, sizeof(diff_z_med));

  // Fence variables
  state->fence_on = 0;
  memset(state->set_points,0,sizeof(state->set_points));
  state->time_fence_init = 0;

  // Initialize IMU data
  //if(imu_mode == 'u' || imu_mode == 'r'){
  //  imu_initialize();
  //}

  return 0;
}

void read_config(char* config){
  // open configuration file
  FILE* conf = fopen(config,"r");
	// holder string
	//char str[1000];

	// read in the quadrotor initial position
	//fscanf(conf,"%s %lf %lf",str,&start[0],&start[1]);
	fscanf(conf, "%lf %lf %lf %lf\n", &(state->pose[0]), &(state->pose[1]), &(state->pose[2]), &(state->pose[3]));
	printf("%lf %lf %lf %lf\n", (state->pose[0]), (state->pose[1]), (state->pose[2]), (state->pose[3]));
	while(fscanf(conf, "%f %f %f %f\n", 
				&(list_of_set_points[num_set_points][0]), 
				&(list_of_set_points[num_set_points][1]),
				&(list_of_set_points[num_set_points][2]),
				&(list_of_set_points[num_set_points][3]) ) != EOF){

		printf("%lf\n", list_of_set_points[num_set_points][2]);
		++num_set_points;
	} 

	for(int i=0; i<3; ++i){
		state->set_points[i] = list_of_set_points[0][i];
	}	
	fclose(conf);
}

/*
void PID_test_Command_handler(struct state* localstate)
{
    float* set_points = localstate->set_points;
    double* pose = localstate->pose;
    channels_t new_msg_1;
    for(int i = 0; i < 8; i++){
      pose[i] = (float) localstate.pose[i];
      set_points[i] = localstate.set_points[i];
    }
    auto_control(&pose, &set_points, new_msg_1.channels,0);

    fprintf(PIDtest_txt,"%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f\n",
	  new_msg_1.channels[0],new_msg_1.channels[1],new_msg_1.channels[2], 
	  new_msg_1.channels[3],
	  set_points[0],set_points[1],set_points[2],
	  set_points[3],set_points[4],set_points[5],set_points[6],
	  set_points[7]);
    fflush(PIDtest_txt);
    



}*/
