// Handler function to either pass through RX commands to Naza or else
// copy computer (autonomous control) commands through to Naza.
#define EXTERN extern

#include "../include/quadcopter_main.h"

float dropoff_time_thre = 0.01; //dropoff time set
int record_flag = 0;
float ini_posx=0.0,ini_posy=0.0,ini_posz=0.0,ini_posyaw=0.0;
float pose[8], set_points[8];

////////////////////////////////////////////////////////////////////////

void channels_handler(const lcm_recv_buf_t *rbuf, const char *channel,
		      const channels_t *msg, void *userdata)
{
  // create a copy of the received message
  channels_t new_msg;
  new_msg.utime = msg->utime;
  new_msg.num_channels = msg->num_channels;
  new_msg.channels = (int16_t*) malloc(msg->num_channels*sizeof(int16_t));
  for(int i = 0; i < msg->num_channels; i++){
    new_msg.channels[i] = msg->channels[i];
  }

  // Copy state to local state struct to minimize mutex lock time
  struct state localstate;
  pthread_mutex_lock(&state_mutex);
  memcpy(&localstate, state, sizeof(struct state));
  pthread_mutex_unlock(&state_mutex);

  //Change fence_on variable
  if (new_msg.channels[msg->num_channels-1] > 1500)
  {
	localstate.fence_on = 1;
  }
  else
  {
	localstate.fence_on = 0;
	record_flag = 0;
  }

  // Decide whether or not to edit the motor message prior to sending it
  // set_points[] array is specific to geofencing.  You need to add code 
  // to compute them for our FlightLab application!!!
  struct motion_capture_obs *mcobs[2];
  pthread_mutex_lock(&mcap_mutex);
  mcobs[0] = mcap_obs;
  if (mcap_obs[1].time > 0)
  {
    mcobs[1] = mcap_obs + 1;
  }
  else
  {
    mcobs[1] = mcap_obs;
  }
  pthread_mutex_unlock(&mcap_mutex);



  // fence on detect,record initial position//
  
  if (localstate.fence_on == 1 && record_flag == 0)
  {
     printf("FENCE ON\n");
     localstate.fence_on = 1;
     ini_posx = (float)localstate.pose[0];
     ini_posy = (float)localstate.pose[1];
     ini_posz = (float)localstate.pose[2];
     ini_posyaw = (float)localstate.pose[3];
     record_flag = 1;
  }
  else 
  {



  }
 

  // fence on detect//

  //read pose,load set point//


  if(localstate.fence_on == 1)
   {
    for(int i = 0; i < 8; i++)
    {
      pose[i] = (float) localstate.pose[i];
      set_points[i] = pose[i];               //set current pos as set point to disable PID
    }
     
     set_points[2] = ini_posz;   //maintain its Z position
     set_points[0] = ini_posx;
     set_points[1] = ini_posy;

     auto_control(pose, set_points, new_msg.channels,0); //begin auto control
     
     //new_msg.channels[0] = msg->channels[0];
     //new_msg.channels[1] = msg->channels[1];
     //new_msg.channels[2] = msg->channels[2];
     new_msg.channels[3] = msg->channels[3];  //guarantee the RC control the non-z part
     new_msg.channels[7] = msg->channels[7]; 
     
     
    // hold position at edge of fence
    // This needs to change - mutex held way too long


    //print("time ecalipse: %")
    //if((mcobs[0]->time - mcobs[1]->time)< dropoff_time_thre)
    //auto_control(pose, set_points, new_msg.channels,0);
    //else
    //  auto_control(pose, set_points, new_msg.channels,1);
    //printf("FENCE ON\n");
    printf ("z setpos: %f, z pos: %f, z from Rc: %d, z from PID: %d, fence:%d\n",ini_posz, pose[2], msg->channels[0],new_msg.channels[0],msg->channels[7]);  //print the diff
   } 
   else{  // Fence off
    // pass user commands through without modifying
    //printf("FENCE OFF\n");
    printf ("ch0:%d,  ch1:%d,  ch2:%d,  ch3:%d,  ch7:%d \n",msg->channels[0],msg->channels[1],msg->channels[2],msg->channels[3],msg->channels[7]);
  }

  // send lcm message to motors
  channels_t_publish((lcm_t *) userdata, "CHANNELS_1_TX", &new_msg);
  
  // Save received (msg) and modified (new_msg) command data to file.
  // NOTE:  Customize as needed (set_points[] is for geofencing)
  fprintf(block_txt,"%ld,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f\n",
	  (long int) msg->utime,msg->channels[0],msg->channels[1],msg->channels[2],
	  msg->channels[3], msg->channels[7],
	  new_msg.channels[0],new_msg.channels[1],new_msg.channels[2], 
	  new_msg.channels[3],new_msg.channels[7],
	  set_points[0],set_points[1],set_points[2],
	  set_points[3],set_points[4],set_points[5],set_points[6],
	  set_points[7]);
  fflush(block_txt);
}
