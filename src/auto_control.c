//
// auto_control:  Function to generate autonomous control PWM outputs.
// 
#define EXTERN extern
#include "../include/quadcopter_main.h"

// Define outer loop controller 
// PWM signal limits and neutral (baseline) settings

// THRUST
#define thrust_PWM_up 1575 // Upper saturation PWM limit.
#define thrust_PWM_base 1500 // Zero z_vela PWM base value. 
#define thrust_PWM_down 1425 // Lower saturation PWM limit. 
   
// ROLL
#define roll_PWM_left 1620  // Left saturation PWM limit.
#define roll_PWM_base 1500  // Zero roll_dot PWM base value. 
#define roll_PWM_right 1380 //Right saturation PWM limit. 

// PITCH
#define pitch_PWM_forward 1620  // Forward direction saturation PWM limit.
#define pitch_PWM_base 1500 // Zero pitch_dot PWM base value. 
#define pitch_PWM_backward 1380 // Backward direction saturation PWM limit. 

// YAW
#define yaw_PWM_ccw 1575 // Counter-Clockwise saturation PWM limit (ccw = yaw left).
#define yaw_PWM_base 1500 // Zero yaw_dot PWM base value. 
#define yaw_PWM_cw 1425 // Clockwise saturation PWM limit (cw = yaw right). 

// Outer loop controller to generate PWM signals for the Naza-M autopilot

float KP_thrust = 150;
float KI_thrust = 0;
float KD_thrust = 0;
int trim_thrust = 1530;
float KP_roll = 100;
float KI_roll = 0;
float KD_roll = 0;
int trim_roll = 1500;
float KP_pitch = 100;
float KI_pitch = 0;
float KD_pitch = 0;
int trim_pitch = 1490;
float KP_yaw = 0.9;
float KI_yaw = 0;
float KD_yaw = 0;
int trim_yaw = 1500;
float error = 0.0, pre_error = 0.0;
int start_flag = 1;
int PID(int mode,float real_pos, float target_pos);
int PID_outputcontrol(int PWM, int Max, int Min);

void auto_control(float *pose, float *set_points, int16_t* channels_ptr, int dropoff)
{
    if (dropoff==0)
    {
        channels_ptr[0] = (int)(PID(0,pose[2],set_points[2]));   //2 for thrust (z)
        channels_ptr[1] = (int)(PID(1,pose[1],set_points[1]));   //1 for change roll, influence y
        channels_ptr[2] = (int)(PID(2,pose[0],set_points[0]));   //2 for change pitch, influence x
        channels_ptr[3] = (int)(PID(3,pose[3],set_points[3]));
    }
    else
    {
        channels_ptr[0] = thrust_PWM_base;
        channels_ptr[1] = roll_PWM_base;
        channels_ptr[2] = pitch_PWM_base;
        channels_ptr[3] = yaw_PWM_base;
    }

        channels_ptr[4] = 0;
        channels_ptr[5] = 0;
        channels_ptr[6] = 0;
        channels_ptr[7] = 0;


  // pose (size 8):  actual {x, y , alt, yaw, xdot, ydot, altdot, yawdot}
  // set_points (size 8):  reference state (you need to set this!) 
  //                       {x, y, alt, yaw, xdot, ydot, altdot, yawdot} 

  // channels_ptr (8-element array of PWM commands to generate in this function)
  // Channels for you to set:
  // [0] = thrust
  // [1] = roll
  // [2] = pitch
  // [3] = yaw


  return;
}





int PID(int mode,float real_pos, float target_pos)
{
    int PWM_output = 1500;

    
    if(start_flag == 1)
	{
	    pre_error = target_pos - real_pos;
            error = target_pos - real_pos;
            start_flag = 2;
	}
	
    else
	{
	    pre_error = error;
            error = target_pos - real_pos;	    
	}	
      
	switch (mode)
	{
		case 0: PWM_output = int(KP_thrust*(error)+trim_thrust+KD_thrust*(error-pre_error));break;
		case 1: PWM_output = int(KP_roll*(error)+trim_roll+KD_roll*(error-pre_error)); break;
		case 2: PWM_output = int(KP_pitch*(error)+trim_pitch+KD_pitch*(error-pre_error));break;
		case 3: PWM_output = int(KP_yaw*(error)+trim_yaw+KD_yaw*(error-pre_error));break;
		default: break;
		
	}

        switch (mode)
	{
		case 0: PWM_output = PID_outputcontrol(PWM_output,1560,1500);break;   //thrust
		case 1: PWM_output = PID_outputcontrol(PWM_output,1520,1450);break;   //roll
		case 2: PWM_output = PID_outputcontrol(PWM_output,1510,1450);break;   //pitch
		case 3: PWM_output = PID_outputcontrol(PWM_output,1550,1450);break;   //yaw
		default: break;
		
	}
	return PWM_output;
}

int PID_outputcontrol(int PWM, int Max, int Min)
{
    if (PWM>=Max)
    {
    	PWM = Max;
    }
    
    if (PWM<=Min)
    {
    	PWM = Min;
    }

    return PWM;


}

